import discord
import mysql.connector
from mysql.connector import Error
import os 
from dotenv import load_dotenv
import random


#---------Load dotenv and get the database information----------------
load_dotenv()

host = os.getenv("host")
db = os.getenv("database")
user = os.getenv("user")
pssw = os.getenv("password")

#---------The init command to create the guild's table------------------- 

def create_guild(guild):
	connection = mysql.connector.connect(host=host,
										 database=db,
										 user=user,
										 password=pssw)

	#--------Querys used to clone the original characters table------------

	query="""CREATE TABLE `{0}` LIKE characters;""".format(guild.id)
	query_2="INSERT INTO `{0}` SELECT * FROM characters;".format(guild.id)

	try:
		cursor = connection.cursor()
		cursor.execute(query)
		cursor.execute(query_2)
		cursor.close()
		connection.close()
		return "**Database created, start rolling ;)**"
	except Error as e:
		return """Error while creating the database please contact support :(
			  	  {}""".format(e)	


def create_refresh_profile(msg):
	connection = mysql.connector.connect(host=host,
										 database=db,
										 user=user,
										 password=pssw)

	query = """INSERT INTO `USERS`(ID,NAME)
			   VALUES({},'{}');""".format(msg.author.id,msg.author)
	
	try:
		cursor = connection.cursor()
		cursor.execute(query)
		cursor.close()
		connection.close()
	except Error as e:
		connection.close()
		print(e)
		return discord.Embed(title="Error", description="""If you haven't created a database for this server yet please
														   use **<init** and wait a few minutes, otherwise contact our support
														   {}""".format(e))



def roll(msg):
	create_refresh_profile(msg)

	connection = mysql.connector.connect(host=host,
										 database=db,
										 user=user,
										 password=pssw)

	query = """SELECT `{0}`.ID, `{0}`.NAME, `{0}`.SERIE, `{0}`.RARITY, `{0}`.IMAGE, 
			   users.name from `{0}` LEFT join users on `{0}`.owner_id = users.ID 
			   where `{0}`.ID = {1} """.format(msg.guild.id,random.randint(1,2))
	
	try:
		cursor = connection.cursor()
		cursor.execute(query)
		gacha = cursor.fetchone()
		cursor.close()
		connection.close()
	except Error as e:
		connection.close()
		return discord.Embed(title="Error", description="""If you haven't created a database for this server yet please
														   use **<init** and wait a few minutes, otherwise contact our support
														   {}""".format(e))

	Id, name, serie, image,owner = gacha[0], gacha[1], gacha[2], gacha[4], gacha[5]
	
	print(owner)

	if owner == None:
		color = int(os.getenv(gacha[3]),16)
	else:
		color = 0x930157

	drop = discord.Embed(title=name, description="""{}
													ID : {}""".format(serie,Id), color = color )
	drop.set_image(url=image)
	
	if owner != None:
		drop.set_footer(text="Owner : {}".format(owner), icon_url="https://previews.123rf.com/images/lkeskinen/lkeskinen1701/lkeskinen170101638/68344293-owned-rubber-stamp.jpg")
	else:
		pass

	return drop



def claim(guild,Claimer,Name,Id):

	connection = mysql.connector.connect(host=host,
										 database=db,
										 user=user,
										 password=pssw)

	query = "UPDATE `{0}` SET `owner_id` = '{1}' WHERE `ID` = '{2}';".format(guild,Claimer.id,Id) 
	
	try:
		cursor = connection.cursor()
		cursor.execute(query)
		cursor.close()
		connection.close()
	except Error as e:
		connection.close()
		return """Error while connecting to the database please wait and try again
				  If the error persists please send this to our support
				  {}""".format(e)

	congrats = "🔥 {} CLAIMED {} 🔥".format(Claimer,Name)
	return congrats



def profile(msg):

	connection = mysql.connector.connect(host=host,
										 database=db,
										 user=user,
										 password=pssw)

	query = "select name from `{}` where owner_id = {}".format(msg.guild.id,msg.author.id)
	
	try:
		cursor = connection.cursor()
		cursor.execute(query)
		profile = cursor.fetchall()
		cursor.close()
		connection.close()
	except Error as e:
		connection.close()
		return discord.Embed(title="Error", description="""If you haven't created a database for this server yet please
														   use **<init** and wait a few minutes, otherwise contact our support
														   {}""".format(e))

	description = ""
	for character in profile:
		description += character[0]+"\n"

	profile = discord.Embed(title = "Characters owned by {}".format(msg.author), 
				            description = description, color = 0x377369 )
	profile.set_thumbnail(url = "https://bolavip.com/__export/1588461137197/sites/bolavip/img/2020/05/02/ibai_llanos_crop1588461136692.jpg_423682103.jpg")

	return profile

