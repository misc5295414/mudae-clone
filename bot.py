# bot.py
import discord
import time
import commands
import random
import os 
from dotenv import load_dotenv

#-------Token is defined---------

load_dotenv()
Token = os.getenv("Discord_Token") 

#-------Rolls cards color are defined------------

Rarities_colors = ("#FFFF00","#A080E0","#0A7DBF","#008000")

#-------Client is defined alongside with it's intents---------

Intents = discord.Intents.default()
Intents.members=True

client = discord.Client(intents = Intents)
prefix = "<"

#---------------Simple connection check for the console-----------

@client.event
async def on_ready():
	print(f'{client.user} has connected to Discord!')

@client.event
async def on_guild_join(guild):
	print(guild)
	# commands.create_guild(guild)

#-------------------------Event handler for the commands-----------------------------------
@client.event
async def on_message(message):

	if message.content.startswith("<init"):
		await message.channel.send(commands.create_guild(message.guild))

	#------------------------------------adds the reaction 💜 for the claims-------------------------------------
	try:
		card = message.embeds[0]
		if message.author == client.user and str(card.color).upper() in Rarities_colors:

			await message.add_reaction("💜")

			def check(reaction, user):
				return user != message.author and str(reaction.emoji) == "💜" and reaction.message.id == message.id

			try:
				reaction, user = await client.wait_for('reaction_add', timeout=30.0, check=check)
			except:
				pass
			else:
				await message.channel.send(commands.claim(message.guild.id,user,card.title,card.description[card.description.index("ID :")+5:]))
				
	except:
		pass

	#----------------------------Handles the <roll command------------------

	if message.content.startswith('{}roll'.format(prefix)):
		await message.channel.send(embed = commands.roll(message))

	#----------------------------Handles the profile command----------------

	elif message.content.startswith('{}profile'.format(prefix)):
		await message.channel.send(embed = commands.profile(message))

client.run(Token)
